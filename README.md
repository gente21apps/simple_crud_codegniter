
////////////////////////////////////  
REPO
////////////////////////////////////  

Instrucciones:  
1) Clone el repo a una carpeta en su sistema, cree un vhost en caso de ser necesario.  
2) Reconstruya la base de datos llamada backend.sql.gz (está en la carpeta ruta).  
3) Cambie los datos de la base de datos en: application/config/database.php.  
4) Navegue a la carpeta /login para comenzar, el usuario de prueba es: Sutanodecual/aguas23.  
5) Para generar un nuevo usuario puede ir directamente a /registro.  

NOTAS:  
1) Este proyecto está basado en CodeIgniter, que contiene tanto el Frontend como el Backend pero separados en modelo MVC.  
2) Las vistas se encuentran en la carpeta application/views.  
3) Los controles están en application/controllers.  
4) Los modelos (que interactúan con la DB) están en application/models.  
5) Usted puede interactuar con los controles como si fuera una API, llamándolos directamente, ejemplos:   
	- http://backend/tasks/tasks_page?_=1520025451538 //para llamar todas las tareas.  
	- http://backend/tasks/task_delete?taskId=2	//para borrar una tarea.  
	- http://backend/tasks/save_task //para guardar una nueva tarea, se debe consumir por POST.  
6) No existe CRUD para los usuarios (no estaba en las especificaciones), pero como ya se dijo, se pueden agregar nuevos.  
7) Para ver las tareas, loguéese como usuario y verá una tabla con las tareas, desde esa página se pueden agregar nuevas tareas y borrar.   
8) No hay forma de editar las tareas (no

¡Gracias por su atención!  
raulrodriguezarias@gmail.com
