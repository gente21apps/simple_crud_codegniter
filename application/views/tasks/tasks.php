<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tareas</title>
	</head>
	<body>

		<div class="container">

			<div class="row">
				<a href="<?= base_url('tasks/task_add') ?>">[+] Agregar tarea</a>
			</div>

			<div class="row">
				<div class="col-md-12">

					<h1>Tareas</h1>

					<table id="tasks-table">
						<thead>
							<tr>
								<td>ID</td>
								<td>Usuario</td>
								<td>Tarea</td>
								<td>Estatus</td>
								<td>Fecha de creación</td>
								<td>Fecha de entrega</td>
								<td>Tiempo en horas</td>
								<td>Borrar</td>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>

				</div>
			</div>
		</div>

	<script type="text/javascript">
		$(document).ready(function() {
		    $('#tasks-table').DataTable({
		    	"ajax": {
		            url : "<?php echo site_url("tasks/tasks_page") ?>",
		            type : 'GET'
		        },
		    });
		});
	</script>

	</body>
</html>