<!DOCTYPE html>
<html lang="en">
<head>
	<title>Agrega tarea</title>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<h3>Genera nueva tarea</h3>
			</div>
			<div class="row">
				<form action="save_task" method="post">
					<div class="col-sm-6">					
	  					<div class="form-group">
	  						<label>Nombre de la tarea</label>
	    						<input type="text" class="form-control" id="name" name="name">
	  					</div>
	  					<div class="form-group">
	  						<label>Estatus</label>
	    						<select id="status" name="status" class="form-control">
	    							<option value="0">Pendiente</option>
	    							<option value="1">En proceso</option>
	    							<option value="2">Terminada</option>
	    						</select>
	  					</div>
	  					<div class="form-group">
	  						<label>fecha de creación</label>
				                <div class='input-group date' id='datetimepicker1'>
				                    <input type='text' class="form-control" name="date_creation" id="date_creation" />
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-calendar"></span>
				                    </span>
				                </div>
			            </div>
			            
	  				</div>
	  				<div class="col-sm-6">					
	  					<div class="form-group">
	  						<label>Usuario</label>
	  						<select class="form-control" id="id_user" name="id_user">
	  							<?php echo $usuarios; ?>
	  						</select>
	  					</div>
	  					<div class="form-group">
	  						<label>Duración en horas</label>
	    						<input type="number" class="form-control" id="time_task" name="time_task">
	  					</div>
	  					<div class="form-group">
	  						<label>fecha de finalización</label>
				                <div class='input-group date' id='datetimepicker2'>
				                    <input type='text' class="form-control" name="date_ending" id="date_ending" />
				                    <span class="input-group-addon">
				                        <span class="glyphicon glyphicon-calendar"></span>
				                    </span>
				                </div>
			            </div>
			            <div class="form-group">
			            	<button type="submit" class="btn btn-primary">Guardar tarea</button>
			            </div>
	  				</div>		
  				</form>
			</div>
		</div>

	<script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
				format: 'YYYY-MM-DD'
            });
            $('#datetimepicker2').datetimepicker({
				format: 'YYYY-MM-DD'
            });
        });
    </script>

	</body>
</html>