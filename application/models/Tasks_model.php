<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 * 
 * @extends CI_Model
 */
class Tasks_model extends CI_Model {
	
	public function get_tasks() {
		$this->load->database();
		$sql = "SELECT tasks.*, users.username FROM tasks INNER JOIN users ON users.id = tasks.id_user";
		$where_condition = '';
		return $this->db->query($sql, $where_condition);
	}

	public function get_users() {
		$this->load->database();
		return $this->db->get('users')->result();
	}

	public function del_task($taskId) {		
		$this->load->database();
		$sql = "DELETE FROM tasks WHERE id = ".$taskId;
		$where_condition = '';
		return $this->db->query($sql, $where_condition);		
	}

	public function save_task($name, $status, $date_creation, $id_user, $time_task, $date_ending) {
		$data = array(
	        'name' => $name,
	        'status' => $status,
	        'date_creation' => $date_creation,
	        'id_user' => $id_user,
	        'time_task' => $time_task,
	        'date_ending' => $date_ending
	    );

		$this->load->database();
	    $this->db->insert('tasks',$data);
	}
	
}

?>