<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User class.
 * 
 * @extends CI_Controller
 */
class Tasks extends CI_Controller {

	public function __construct() {
        Parent::__construct();
        $this->load->model("Tasks_model");
    }

    public function tasks() {
        $this->load->view("tasks/tasks.php", array());
    }

    public function save_task(){
    	$this->Tasks_model->save_task(
    		$this->input->post("name"),
    		$this->input->post("status"),
    		$this->input->post("date_creation"),
    		$this->input->post("id_user"),
    		$this->input->post("time_task"),
    		$this->input->post("date_ending")
    	);
    	$this->load->helper('url');
    	$this->load->view('header');
		$this->load->view('tasks/tasks');
		$this->load->view('footer');
    }

    public function task_add(){

    	$usuarios = json_decode(json_encode($this->Tasks_model->get_users()), true);

    	$opciones = '';
    	for ($i=0; $i < sizeof($usuarios); $i++) { 
    		$opciones .= '<option value="'.$usuarios[$i]['id'].'">'.$usuarios[$i]['username'].'</option>';
    	}

    	$data['usuarios'] = $opciones;

    	//redirectéame
        $this->load->helper('url');
        $this->load->view('header');
		$this->load->view('tasks/task_add', $data);
		$this->load->view('footer');
    }

    public function task_delete(){
    	if ( isset($_GET['taskId']) ) {
	        $taskId = $_GET['taskId'];
	        $this->Tasks_model->del_task($taskId);

	        //redirectéame
	        $this->load->helper('url');
	        $this->load->view('header');
			$this->load->view('tasks/tasks');
			$this->load->view('footer');

	    } else {
	    	die('>No sé cuál tarea borrar');
	    }
    }

    public function tasks_page() {

          // Datatables Variables
          $draw = intval($this->input->get("draw"));
          $start = intval($this->input->get("start"));
          $length = intval($this->input->get("length"));


          $tasks = $this->Tasks_model->get_tasks();

          $data = array();

          foreach($tasks->result() as $tarea) {

          	$estatus = 'pendiente';
          	switch (intval($tarea->status)) {
          		case 1:
          			$estatus = 'en proceso';
          			break;          		
          		case 2:
          			$estatus = 'terminada';
          			break;
          	}

          	/*
			base_url('tasks/task_add')
          	*/
          	$this->load->helper('url');
          	$link_borrar = base_url('tasks/task_delete?taskId='.$tarea->id);
          	$botones_editar = "<a href='".$link_borrar."'>[-] borrar</a>";

           	$data[] = array(
	            $tarea->id,
	            $tarea->username,
	            $tarea->name,
	            $estatus,
	            $tarea->date_creation,
	            $tarea->date_ending,
	            $tarea->time_task,
	            $botones_editar
            );
          }

          $output = array(
               "draw" => $draw,
                 "recordsTotal" => $tasks->num_rows(),
                 "recordsFiltered" => $tasks->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
          exit();
     }
	
	
}
