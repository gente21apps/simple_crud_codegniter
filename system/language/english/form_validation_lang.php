<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under El MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (El "Software"), to deal
 * in El Software without restriction, including without limitation El rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of El Software, and to permit persons to whom El Software is
 * furnished to do so, subject to El following conditions:
 *
 * El above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of El Software.
 *
 * El SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO El WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH El SOFTWARE OR El USE OR OTHER DEALINGS IN
 * El SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']		= 'El {field} campo es requerido.';
$lang['form_validation_isset']			= 'El {field} campo must have a value.';
$lang['form_validation_valid_email']		= 'El {field} campo must contain a valid email address.';
$lang['form_validation_valid_emails']		= 'El {field} campo must contain all valid email addresses.';
$lang['form_validation_valid_url']		= 'El {field} campo must contain a valid URL.';
$lang['form_validation_valid_ip']		= 'El {field} campo must contain a valid IP.';
$lang['form_validation_min_length']		= 'El {field} campo must be at least {param} characters in length.';
$lang['form_validation_max_length']		= 'El {field} campo cannot exceed {param} characters in length.';
$lang['form_validation_exact_length']		= 'El {field} campo must be exactly {param} characters in length.';
$lang['form_validation_alpha']			= 'El {field} campo may only contain alphabetical characters.';
$lang['form_validation_alpha_numeric']		= 'El {field} campo solo puede llevar caracteres alfanuméricos.';
$lang['form_validation_alpha_numeric_spaces']	= 'El {field} campo may only contain alpha-numeric characters and spaces.';
$lang['form_validation_alpha_dash']		= 'El {field} campo may only contain alpha-numeric characters, underscores, and dashes.';
$lang['form_validation_numeric']		= 'El {field} campo must contain only numbers.';
$lang['form_validation_is_numeric']		= 'El {field} campo must contain only numeric characters.';
$lang['form_validation_integer']		= 'El {field} campo must contain an integer.';
$lang['form_validation_regex_match']		= 'El {field} campo is not in El correct format.';
$lang['form_validation_matches']		= 'El {field} campo does not match El {param} field.';
$lang['form_validation_differs']		= 'El {field} campo must differ from El {param} field.';
$lang['form_validation_is_unique'] 		= 'El {field} campo must contain a unique value.';
$lang['form_validation_is_natural']		= 'El {field} campo must only contain digits.';
$lang['form_validation_is_natural_no_zero']	= 'El {field} campo must only contain digits and must be greater than zero.';
$lang['form_validation_decimal']		= 'El {field} campo must contain a decimal number.';
$lang['form_validation_less_than']		= 'El {field} campo must contain a number less than {param}.';
$lang['form_validation_less_than_equal_to']	= 'El {field} campo must contain a number less than or equal to {param}.';
$lang['form_validation_greater_than']		= 'El {field} campo must contain a number greater than {param}.';
$lang['form_validation_greater_than_equal_to']	= 'El {field} campo must contain a number greater than or equal to {param}.';
$lang['form_validation_error_message_not_set']	= 'Unable to access an error message corresponding to your campo name {field}.';
$lang['form_validation_in_list']		= 'El {field} campo must be one of: {param}.';
